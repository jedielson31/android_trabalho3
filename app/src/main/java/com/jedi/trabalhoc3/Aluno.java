package com.jedi.trabalhoc3;

import java.util.List;

/**
 * Created by jedi on 27/03/2017.
 *
 * Classe Aluno
 */

public class Aluno {

    private String nome;
    private String celular;
    private String email;
    private  String matricula;
    private String unidade;
    private List<String> interesses;

    public Aluno(String nome, String celular, String email, String matricula, String unidade, List<String> interesses) {
        this.nome = nome;
        this.celular = celular;
        this.email = email;
        this.matricula = matricula;
        this.unidade = unidade;
        this.interesses = interesses;
    }

    public String getNome() {
        return nome;
    }

    public String getCelular() {
        return celular;
    }

    public String getEmail() {
        return email;
    }

    public String getMatricula() {
        return matricula;
    }

    public String getUnidade() {
        return unidade;
    }

    public List<String> getInteresses() {
        return interesses;
    }
}
