package com.jedi.trabalhoc3;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;

@SuppressWarnings("ALL")
public class MainActivity extends AppCompatActivity implements RadioGroup.OnCheckedChangeListener, Validator.ValidationListener, CompoundButton.OnCheckedChangeListener {

    //region Properties
    //region View Bindings
    @BindView(R.id.edit_text_nome)
    @NotEmpty(messageResId = R.string.activity_main_edit_text_nome_error_not_empty)
    protected EditText editTextNome;

    @BindView(R.id.edit_text_celular)
    @NotEmpty(messageResId = R.string.activity_main_edit_text_celular_error_not_empty)
    @Length(min = 10, max = 15, messageResId = R.string.activity_main_edit_text_celular_error_lenght)
    protected EditText editTextCelular;

    @BindView(R.id.edit_text_email)
    @NotEmpty(messageResId = R.string.activity_main_edit_text_email_error_not_empty)
    @Email(messageResId = R.string.activity_main_edit_text_email_error_email)
    protected EditText editTextEmail;

    @BindView(R.id.edit_text_matricula)
    @NotEmpty(messageResId = R.string.activity_main_edit_text_celular_matricula_not_empty)
    protected EditText editTextMatricula;

    @BindView(R.id.check_box_ciencia_aluno)
    protected CheckBox checkBoxCienciaAluno;

    @BindView(R.id.button_pronto)
    protected Button buttonPronto;

    @BindView(R.id.radio_group_unidades)
    protected RadioGroup radioGroupUnidades;

    @BindView(R.id.text_view_resumo)
    protected TextView textViewResumo;

    @BindViews({R.id.radio_button_hauer, R.id.radio_button_batel, R.id.radio_button_angelo, R.id.radio_button_osorio, R.id.radio_button_ecoville})
    protected List<RadioButton> radioButtons;

    @BindViews({R.id.check_box_esportes, R.id.check_box_cinema, R.id.check_box_leitura, R.id.check_box_teatro, R.id.check_box_viagens})
    protected List<CheckBox> checkBoxes;

    @BindView(R.id.text_view_interesses_error)
    protected TextView textViewPreferenciasError;
    //endregion

    //region Others
    protected RadioButton selectedRadioButtonUnidade;
    Validator validator;
    //endregion
    //endregion

    //region Lifecycle
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.validator = new Validator(this);
        this.validator.setValidationListener(this);

        ButterKnife.bind(this);
        this.radioGroupUnidades.setOnCheckedChangeListener(this);
        this.radioGroupUnidades.check(R.id.radio_button_hauer);
        this.editTextCelular.addTextChangedListener(new FoneTextWatcher(new WeakReference<>(this.editTextCelular)));
        this.checkBoxCienciaAluno.setOnCheckedChangeListener(this);
    }

    //endregion

    //region View Listeners
    @OnClick(R.id.button_pronto)
    protected void ButtonProntoClick(View v) {
        if (!hasSelectedAtLeastOneInterest()) {
            this.textViewPreferenciasError.setVisibility(View.VISIBLE);
        } else {
            this.textViewPreferenciasError.setVisibility(View.GONE);
        }
        this.validator.validate();
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(isChecked){
            this.buttonPronto.setEnabled(true);
            return;
        }

        this.buttonPronto.setEnabled(false);
        this.textViewResumo.setText("");
    }

    @Override
    public void onCheckedChanged(RadioGroup group, final int checkedId) {
        this.selectedRadioButtonUnidade = null;
        for (RadioButton rb : this.radioButtons) {
            if (rb.getId() == checkedId) {
                this.selectedRadioButtonUnidade = rb;
            }
        }
    }
    //endregion

    //region View Logic
    @NonNull
    private Aluno bindValues() {

        String unidade = this.selectedRadioButtonUnidade != null ? this.selectedRadioButtonUnidade.getText().toString() : null;

        Aluno aluno = new Aluno(this.editTextNome.getText().toString(),
                this.editTextCelular.getText().toString(),
                this.editTextEmail.getText().toString(),
                this.editTextMatricula.getText().toString(),
                unidade,
                this.bindInteresses());

        return aluno;
    }

    @NonNull
    private List<String> bindInteresses() {
        List<String> retorno = new ArrayList<>();
        for (CheckBox cb : this.checkBoxes) {
            if (cb.isChecked()) {
                retorno.add(cb.getText().toString());
            }
        }
        return retorno;
    }

    @NonNull
    private String buildMensagemSucesso(@NonNull Aluno aluno) {
        String base = this.getString(R.string.activity_main_text_view_success_text);

        base = base.replace("@nome@", aluno.getNome());
        base = base.replace("@unidade@", aluno.getUnidade());
        base = base.replace("@matricula@", aluno.getMatricula());
        base = base.replace("@email@", aluno.getEmail());
        base = base.replace("@celular@", aluno.getCelular());
        base = base.replace("@atividades@", this.buildInteressesText(aluno));

        return base;
    }

    @NonNull
    private String buildInteressesText(@NonNull Aluno aluno) {
        StringBuilder retorno = new StringBuilder();
        List<String> interesses = aluno.getInteresses();

        if (interesses.isEmpty()) {
            retorno.append("Não informado.");
            return retorno.toString();
        }

        int posicaoE = interesses.size() - 2;
        for (int i = 0; i < interesses.size(); i++) {
            retorno.append(interesses.get(i));

            if (posicaoE == i) {
                retorno.append(" e ");
                continue;
            }

            if (i == interesses.size() - 1) {
                retorno.append(".");
                continue;
            }

            retorno.append(", ");
        }

        return retorno.toString();
    }
    //endregion

    //region Validations
    @Override
    public void onValidationSucceeded() {
        Aluno aluno = this.bindValues();
        this.textViewResumo.setText(this.buildMensagemSucesso(aluno));
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {

        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }


    }

    public boolean hasSelectedAtLeastOneInterest() {

        for (CheckBox cb : this.checkBoxes) {
            if (cb.isChecked()) {
                return true;
            }
        }

        return false;
    }
    //endregion
}